# Welcome to the moped documentation

- [Genome reconstruction tutorial](01-ecoli-from-genome)
- [Interaction with moped](02-interaction)
- [Topology tutorial](03-topology)
- [Kinetic model tutorial](04-glycolysis-kinetic-model)

