from __future__ import annotations

from concurrent.futures import ProcessPoolExecutor
from pathlib import Path
from typing import Iterable

from moped import Model


def test_meneco_example() -> None:
    def fn(model: Model, db: Model, seeds: set[str], targets: Iterable[str]) -> None:
        assert model.get_gapfilling_reactions(db, seeds, targets=targets)

    model = Model()
    db = Model()
    model.read_from_sbml(Path(__file__).parent / "data" / "ectocyc.sbml")
    db.read_from_sbml(Path(__file__).parent / "data" / "metacyc_16-5.sbml")

    seeds = {
        "Acceptor",
        "ACP",
        "ADP",
        "AMMONIA",
        "AMP",
        "ATP",
        "BIOTIN",
        "CA+2",
        "CARBON-DIOXIDE",
        "CDP-12921",
        "CL-",
        "CO-A",
        "CPD0-1695",
        "CPD0-2394",
        "CPD-3",
        "CPD-315",
        "CU+",
        "CU+2",
        "Donor-H2",
        "EDTA",
        "GDP",
        "GTP",
        "HYDROGEN-PEROXIDE",
        "K+",
        "MG+2",
        "NA+",
        "NAD-P-OR-NOP",
        "NAD",
        "NADH-P-OR-NOP",
        "NADH",
        "NADP",
        "NADPH",
        "NITRATE",
        "OXYGEN-MOLECULE",
        "Pi",
        "PPI",
        "PROTON",
        "SULFATE",
        "THIAMINE",
        "UDP",
        "Vitamins-B12",
        "COB-I-ALAMIN",
        "WATER",
        "ZN+2",
    }

    with ProcessPoolExecutor() as p:
        p.map(
            lambda targets: fn(model, db, seeds, targets),
            [
                ["CPD-9247"],
                ["CPD-9245"],
                ["ARACHIDIC_ACID"],
                ["PALMITATE"],
                ["ARACHIDONIC_ACID"],
                ["CPD-9247"],
                ["LINOLEIC_ACID"],
                ["OLEATE-CPD"],
                ["CPD-7836"],
                ["CPD-8117"],
                ["STEARIC_ACID"],
                ["CPD-8120"],
                # ["HIS"],  # why doesn't this work?
                ["D-ALANINE"],
            ],
        )
