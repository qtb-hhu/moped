import pytest
from moped import Compound, Model, Reaction


def test_linear_chain() -> None:
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}

    model = Model(
        compounds=(
            Compound(base_id="cpd1", compartment="CYTOSOL"),
            Compound(base_id="cpd2", compartment="CYTOSOL"),
            Compound(base_id="cpd3", compartment="CYTOSOL"),
            Compound(base_id="cpd4", compartment="CYTOSOL"),
            Compound(base_id="cpd5", compartment="CYTOSOL"),
        ),
        reactions=(
            Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
            # Reaction(id="rxn2", stoichiometries={"cpd2_c": -1, "cpd3_c": 1}),
            # Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
            Reaction(id="rxn5", stoichiometries={"cpd4_c": -1, "cpd5_c": 1}),
        ),
        compartments=compartments,
    )
    db = Model(
        compounds=(
            Compound(base_id="cpd1", compartment="CYTOSOL"),
            Compound(base_id="cpd2", compartment="CYTOSOL"),
            Compound(base_id="cpd3", compartment="CYTOSOL"),
            Compound(base_id="cpd4", compartment="CYTOSOL"),
        ),
        reactions=(
            Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
            Reaction(id="rxn2", stoichiometries={"cpd2_c": -1, "cpd3_c": 1}),
            Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
        ),
        compartments=compartments,
    )

    assert model.get_gapfilling_reactions(db, seed=["cpd1_c"], targets=["cpd3_c"]) == [
        "rxn2"
    ]
    assert model.get_gapfilling_reactions(db, seed=["cpd1_c"], targets=["cpd4_c"]) == [
        "rxn2",
        "rxn3",
    ]
    assert model.get_gapfilling_reactions(db, seed=["cpd1_c"], targets=["na"]) == []
    assert model.get_gapfilling_reactions(db, seed=["cpd1_c"], targets=["cpd5_c"]) == [
        "rxn2",
        "rxn3",
    ]


def test_linear_chain_gapfilling() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction("rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(id="rxn2", stoichiometries={"cpd2_c": -1, "cpd3_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)

    gapfilling_reactions = m.get_gapfilling_reactions(
        reference_model=db, seed=["cpd1_c"], targets=["cpd3_c"], verbose=False
    )
    m2 = m.copy()
    m2.gapfilling(reference_model=db, seed=["cpd1_c"], targets=["cpd3_c"], verbose=False)
    assert set(m.reactions) | set(gapfilling_reactions) == set(m2.reactions)


def test_linear_chain_fail_on_missing() -> None:
    model_compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
    )
    db_compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction("rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    m = Model(
        compounds=model_compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(
        compounds=db_compounds,
        reactions=db_reactions,
        compartments=compartments,
    )
    with pytest.warns(UserWarning):
        m.get_gapfilling_reactions(
            reference_model=db,
            seed=["cpd1_c"],
            targets=["cpd3_c"],
            verbose=False,
        )


def test_linear_chain_water_addition() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
        Compound(base_id="cpd4", compartment="CYTOSOL"),
        Compound(base_id="WATER", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction("rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(
            id="rxn2",
            stoichiometries={"cpd2_c": -1, "WATER_c": -1, "cpd3_c": 1},
        ),
        Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)
    reaction_ids = m.get_gapfilling_reactions(
        reference_model=db,
        seed=["cpd1_c", "WATER_c"],
        targets=["cpd4_c"],
        verbose=False,
    )
    assert set(reaction_ids) == set(["rxn3", "rxn2"])


def test_reversible2() -> None:
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    db = Model(compartments=compartments)
    db.add_compounds(
        (
            Compound(base_id="cpd1", compartment="CYTOSOL"),
            Compound(base_id="cpd2", compartment="CYTOSOL"),
            Compound(base_id="cpd3", compartment="CYTOSOL"),
        )
    )
    db.add_reactions(
        [
            Reaction(
                id="v1",
                stoichiometries={"cpd2_c": -1, "cpd1_c": 1},
                bounds=(-1000, 1000),
            ),
            Reaction(
                id="v2",
                stoichiometries={"cpd2_c": -1, "cpd3_c": 1},
                bounds=(0, 1000),
            ),
        ]
    )

    m = db.copy()
    m.remove_reaction("v1")

    assert not m.get_gapfilling_reactions(db, ["cpd1_c"], ["cpd3_c"])
    db.reversibility_duplication()
    assert m.get_gapfilling_reactions(db, ["cpd1_c"], ["cpd3_c"]) == ["v1__rev__"]


def test_linear_chain_reversible() -> None:
    compartments = {"CYTOSOL": "c"}

    model = Model(
        compounds=(
            Compound(base_id="cpd1", compartment="CYTOSOL"),
            Compound(base_id="cpd2", compartment="CYTOSOL"),
            Compound(base_id="cpd3", compartment="CYTOSOL"),
        ),
        reactions=(
            # Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}, bounds=(-1000, 1000)),
            Reaction(
                id="rxn2",
                stoichiometries={"cpd2_c": -1, "cpd3_c": 1},
                bounds=(-1000, 1000),
            ),
        ),
        compartments=compartments,
    )
    db = Model(
        compounds=(
            Compound(base_id="cpd1", compartment="CYTOSOL"),
            Compound(base_id="cpd2", compartment="CYTOSOL"),
            Compound(base_id="cpd3", compartment="CYTOSOL"),
        ),
        reactions=(
            Reaction(
                id="rxn1",
                stoichiometries={"cpd1_c": -1, "cpd2_c": 1},
                bounds=(-1000, 1000),
            ),
            Reaction(
                id="rxn2",
                stoichiometries={"cpd2_c": -1, "cpd3_c": 1},
                bounds=(-1000, 1000),
            ),
        ),
        compartments=compartments,
    )

    # Fail before reversibility duplication
    assert not model.get_gapfilling_reactions(db, seed=["cpd3_c"], targets=["cpd1_c"])

    # Succeed now
    model.reversibility_duplication()
    db.reversibility_duplication()
    assert model.get_gapfilling_reactions(db, seed=["cpd3_c"], targets=["cpd1_c"]) == [
        "rxn1__rev__"
    ]


def test_linear_chain_fail_on_reverse_with_duplication() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(id="rxn2", stoichiometries={"cpd3_c": -1, "cpd2_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)
    db.reversibility_duplication()
    assert [] == m.get_gapfilling_reactions(
        reference_model=db, seed=["cpd1_c"], targets=["cpd3_c"], verbose=False
    )


def test_linear_chain_cofactor_duplicated() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
        Compound(base_id="cpd4", compartment="CYTOSOL"),
        Compound(base_id="ATP", compartment="CYTOSOL"),
        Compound(base_id="ADP", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(
            id="rxn2",
            stoichiometries={
                "cpd2_c": -1,
                "ADP_c": -1,
                "cpd3_c": 1,
                "ATP_c": 1,
            },
        ),
        Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)
    db.cofactor_pairs = {"ATP_c": "ADP_c"}
    db.cofactor_duplication()
    m.cofactor_pairs = {"ATP_c": "ADP_c"}
    reaction_ids = m.get_gapfilling_reactions(
        reference_model=db,
        seed=["cpd1_c"] + m.get_weak_cofactor_duplications(),
        targets=["cpd4_c"],
        verbose=False,
    )
    assert set(reaction_ids) == set(["rxn2__cof__", "rxn3"])


def test_linear_chain_include_cofactor_duplicated() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
        Compound(base_id="cpd4", compartment="CYTOSOL"),
        Compound(base_id="ATP", compartment="CYTOSOL"),
        Compound(base_id="ADP", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(
            id="rxn2",
            stoichiometries={
                "cpd2_c": -1,
                "ADP_c": -1,
                "cpd3_c": 1,
                "ATP_c": 1,
            },
        ),
        Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    m.add_cofactor_pair(strong_cofactor_base_id="ATP_c", weak_cofactor_base_id="ADP_c")
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)
    db.cofactor_pairs = {"ATP_c": "ADP_c"}
    db.cofactor_duplication()
    m.cofactor_pairs = {"ATP_c": "ADP_c"}
    reaction_ids = m.get_gapfilling_reactions(
        reference_model=db,
        seed=["cpd1_c"],
        targets=["cpd4_c"],
        include_weak_cofactors=True,
        verbose=False,
    )
    assert set(reaction_ids) == set(["rxn2__cof__", "rxn3"])


def test_linear_chain_cofactor_duplicated_fail_without_duplication() -> None:
    compounds = (
        Compound(base_id="cpd1", compartment="CYTOSOL"),
        Compound(base_id="cpd2", compartment="CYTOSOL"),
        Compound(base_id="cpd3", compartment="CYTOSOL"),
        Compound(base_id="cpd4", compartment="CYTOSOL"),
        Compound(base_id="ATP", compartment="CYTOSOL"),
        Compound(base_id="ADP", compartment="CYTOSOL"),
    )
    compartments = {"CYTOSOL": "c", "PERIPLASM": "p", "EXTRACELLULAR": "e"}
    model_reactions = (Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),)
    db_reactions = (
        Reaction(id="rxn1", stoichiometries={"cpd1_c": -1, "cpd2_c": 1}),
        Reaction(
            id="rxn2",
            stoichiometries={
                "cpd2_c": -1,
                "ADP_c": -1,
                "cpd3_c": 1,
                "ATP_c": 1,
            },
        ),
        Reaction(id="rxn3", stoichiometries={"cpd3_c": -1, "cpd4_c": 1}),
    )
    m = Model(
        compounds=compounds,
        reactions=model_reactions,
        compartments=compartments,
    )
    db = Model(compounds=compounds, reactions=db_reactions, compartments=compartments)
    db.cofactor_pairs = {"ATP_c": "ADP_c"}
    m.cofactor_pairs = {"ATP_c": "ADP_c"}

    reaction_ids = m.get_gapfilling_reactions(
        reference_model=db,
        seed=["cpd1_c"] + m.get_weak_cofactor_duplications(),
        targets=["cpd4_c"],
        verbose=False,
    )

    assert reaction_ids == []
