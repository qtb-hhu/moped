from moped import Reaction


def test_reverse_stoichiometries_stoichiometries() -> None:
    rxn = Reaction("RXN001", stoichiometries={"X": -1, "Y": 1})
    rxn.reverse_stoichiometry()
    assert rxn.stoichiometries == {"X": 1, "Y": -1}


def test_reverse_stoichiometries_bounds() -> None:
    rxn = Reaction("RXN001", stoichiometries={"X": -1, "Y_e": 1}, bounds=(-50, 100))
    rxn.reverse_stoichiometry()
    assert rxn.bounds == (-100, 50)


def test_reverse_stoichiometries_gibbs0() -> None:
    rxn = Reaction("RXN001", stoichiometries={"X": -1, "Y_e": 1}, gibbs0=-1)
    rxn.reverse_stoichiometry()
    assert rxn.gibbs0 == 1
