from moped import Reaction


def test_replace_substrate() -> None:
    v = Reaction("v", stoichiometries={"x": -1, "y": 1})
    v.replace_compound("x", "x1")
    assert v.stoichiometries == {"x1": -1, "y": 1}


def test_replace_product() -> None:
    v = Reaction("v", stoichiometries={"x": -1, "y": 1})
    v.replace_compound("y", "y1")
    assert v.stoichiometries == {"x": -1, "y1": 1}
