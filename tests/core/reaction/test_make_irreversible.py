from moped import Reaction


def test_reaction_make_irreversible_upper_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(0, 1000))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (0, 1000)


def test_reaction_make_irreversible_lower_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 0))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (-1000, 0)


def test_reaction_make_irreversible_upper_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(0, 1))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (0, 1)


def test_reaction_make_irreversible_lower_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 0))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (-1, 0)


def test_reaction_make_irreversible_reversible_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 1000))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (0, 1000)


def test_reaction_make_irreversible_reversible_lower_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 1000))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (0, 1000)


def test_reaction_make_irreversible_reversible_upper_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 1))
    r.make_irreversible()
    assert not r.reversible
    assert r.bounds == (0, 1)
