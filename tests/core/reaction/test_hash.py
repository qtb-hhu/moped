from moped import Reaction


def test_hash() -> None:
    rxn = Reaction(id="RXN001_c")
    assert hash(rxn) == hash("RXN001_c")
