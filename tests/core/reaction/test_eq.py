from moped import Compound, Reaction


def test_eq() -> None:
    r1 = Reaction(id="rxn1")
    r2 = Reaction(id="rxn1")
    assert r1 == r2


def test_not_eq() -> None:
    r1 = Reaction(id="rxn1")
    r2 = Reaction(id="rxn2")
    assert not r1 == r2


def test_not_equal_differnent_objects() -> None:
    rxn = Reaction(id="rxn1")
    cpd = Compound(id="cpd1", base_id="cpd1", compartment="c")
    assert rxn != cpd
