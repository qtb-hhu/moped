from moped import Reaction


def test_initialize_id() -> None:
    assert Reaction("RXN001").id == "RXN001"


def test_initialize_name() -> None:
    rxn = Reaction("RXN001", name="Reaction")
    assert rxn.name == "Reaction"


def test_initialize_stoichiometries() -> None:
    rxn = Reaction("RXN001", stoichiometries={"X": -1, "Y": 1})
    assert rxn.stoichiometries == {"X": -1, "Y": 1}


def test_initialize_bounds() -> None:
    rxn = Reaction("RXN001", bounds=(-1000, 1000))
    assert rxn.bounds == (-1000, 1000)


def test_initialize_reversible_default() -> None:
    rxn = Reaction("RXN001")
    assert rxn.reversible is False


def test_initialize_reversible_true() -> None:
    rxn = Reaction("RXN001", bounds=(-1000, 1000))
    assert rxn.reversible is True


def test_initialize_reversible_false() -> None:
    rxn = Reaction("RXN001", bounds=(0, 1000))
    assert rxn.reversible is False


def test_initialize_gibbs0() -> None:
    rxn = Reaction("RXN001", gibbs0=1)
    assert rxn.gibbs0 == 1


def test_initialize_ec() -> None:
    rxn = Reaction("RXN001", ec="123")
    assert rxn.ec == "123"


def test_initialize_pathways() -> None:
    rxn = Reaction("RXN001", pathways={"PWY-101"})
    assert rxn.pathways == {"PWY-101"}


def test_transmembrane_both_none() -> None:
    rxn = Reaction(
        id="RXN001_c",
        base_id="RXN001",
        stoichiometries={"X": -1, "Y": 1},
    )
    assert not rxn.transmembrane


def test_transmembrane_both_cytosol() -> None:
    rxn = Reaction(
        id="RXN001_c",
        base_id="RXN001",
        stoichiometries={"X_c": -1, "Y_c": 1},
    )
    assert not rxn.transmembrane


def test_transmembrane_different_compartments() -> None:
    rxn = Reaction(
        id="RXN001_c",
        base_id="RXN001",
        stoichiometries={"X_c": -1, "Y_p": 1},
    )
    assert rxn.transmembrane


def test_reaction_reversibility_from_bounds_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 1000))
    assert r.reversible


def test_reaction_reversibility_from_bounds_upper_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 1))
    assert r.reversible


def test_reaction_reversibility_from_bounds_lower_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 1))
    assert r.reversible


def test_reaction_reversibility_from_bounds_upper_zero() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 0))
    assert not r.reversible


def test_reaction_reversibility_from_bounds_lower_zero() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(0, 1000))
    assert not r.reversible
