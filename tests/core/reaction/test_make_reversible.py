from moped import Reaction


def test_reaction_make_reversible_upper_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(0, 1000))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1000, 1000)


def test_reaction_make_reversible_lower_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 0))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1000, 1000)


def test_reaction_make_reversible_upper_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(0, 1))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1, 1)


def test_reaction_make_reversible_lower_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 0))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1, 1)


def test_reaction_make_reversible_already_reversible_max() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 1000))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1000, 1000)


def test_reaction_make_reversible_already_reversible_lower_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1, 1000))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1, 1000)


def test_reaction_make_reversible_already_reversible_upper_one() -> None:
    r = Reaction(id="rxn", stoichiometries={"x": -1, "y": 1}, bounds=(-1000, 1))
    r.make_reversible()
    assert r.reversible
    assert r.bounds == (-1000, 1)
