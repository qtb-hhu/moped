from moped import Compound


def test_initialization_cytosol() -> None:
    cpd = Compound(base_id="CPD001", compartment="CYTOSOL")
    assert cpd.base_id == "CPD001"
    assert cpd.compartment == "CYTOSOL"


def test_initialization_compartment_periplasm() -> None:
    cpd = Compound(base_id="CPD001", compartment="PERIPLASM")
    assert cpd.base_id == "CPD001"
    assert cpd.compartment == "PERIPLASM"


def test_initialization_compartment_extracellular() -> None:
    cpd = Compound(base_id="CPD001", compartment="EXTRACELLULAR")
    assert cpd.base_id == "CPD001"
    assert cpd.compartment == "EXTRACELLULAR"


def test_initialize_name() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", name="compound")
    assert c.name == "compound"


def test_initialize_formula() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", formula={"C": 1})
    assert c.formula == {"C": 1}


def test_initialize_charge() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", charge=1)
    assert c.charge == 1


def test_initialize_gibbs0() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", gibbs0=1)
    assert c.gibbs0 == 1


def test_initialize_smiles() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", smiles="CN=C=O")
    assert c.smiles == "CN=C=O"


def test_initialize_types() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", types=["small-molecule"])
    assert c.types == ["small-molecule"]


def test_initialize_in_reaction() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", in_reaction={"rxn-1"})
    assert c.in_reaction == {"rxn-1"}
