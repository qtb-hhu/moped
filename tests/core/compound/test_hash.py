from moped import Compound


def test_hash() -> None:
    cpd = Compound(base_id="CPD001", id="CPD001", compartment="CYTOSOL")
    assert hash(cpd) == hash(cpd.id)
