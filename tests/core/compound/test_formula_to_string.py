from moped import Compound


def test_formula_to_string() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", formula={"C": 1})
    assert c.formula_to_string() == "C1"


def test_formula_to_string_two_atoms() -> None:
    c = Compound(base_id="CPD001", compartment="CYTOSOL", formula={"C": 1, "H": 12})
    assert c.formula_to_string() == "C1H12"
