from moped import Compound, Reaction


def test_eq() -> None:
    cpd1 = Compound(base_id="CPD001", id="CPD001", compartment="CYTOSOL")
    cpd2 = Compound(base_id="CPD001", id="CPD001", compartment="CYTOSOL")
    assert cpd1 == cpd2


def test_not_eq() -> None:
    cpd1 = Compound(base_id="CPD001", id="CPD001", compartment="CYTOSOL")
    cpd2 = Compound(base_id="CPD001", id="CPD002", compartment="CYTOSOL")
    assert not cpd1 == cpd2


def test_not_equal_differnent_objects() -> None:
    cpd = Compound(id="cpd1", base_id="cpd1", compartment="c")
    rxn = Reaction(id="rxn1")
    assert not cpd == rxn
