export LC_ALL=C

echo "isort"
isort moped/
isort tests/
echo "black"
black moped/
black tests/
echo "flake8"
flake8 moped/
flake8 tests/
echo "mypy"
mypy moped/
mypy tests/
echo "pytest"
pytest tests/